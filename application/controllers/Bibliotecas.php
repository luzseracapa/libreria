<?php
  class Bibliotecas extends CI_Controller
  {
    function __construct()
    {
        parent:: __construct();
    }
    public function conocenos(){
        $this->load->view('header');
        $this->load->view('bibliotecas/conocenos');
        $this->load->view('footer');
    }

    public function cuentos(){
        $this->load->view('header');
        $this->load->view('bibliotecas/cuentos');
        $this->load->view('footer');
    }

    public function libros(){
        $this->load->view('header');
        $this->load->view('bibliotecas/libros');
        $this->load->view('footer');
    }

    public function revistas(){
        $this->load->view('header');
        $this->load->view('bibliotecas/revistas');
        $this->load->view('footer');
    }
  }
?>