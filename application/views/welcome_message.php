<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/ma.jpg" alt="maxx" align="center">
      <div class="carousel-caption">
        <h3>CUENTOS</h3>
        <p>LA is always so much fun!</p>
      </div>
    </div>
<br>
    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/max.jpg" alt="maa" align="center">
      <div class="carousel-caption">
        <h3>INFANTILES</h3>
        <p>Thank you, Chicago!</p>
      </div>
    </div>
<br>
    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/images/10.jpg" alt="marx" align="center">
      <div class="carousel-caption">
        <h3>LATORMENTA</h3>
        <p>We love the Big Apple!</p>
      </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>